package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1
    else if (c > r - 1 || c < 0) 0
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    object AllDone extends Exception {}
    var lt = 0
    def checkBrace(sym: Char) {
      if (sym == '(') { lt = lt + 1 }
      if (sym == ')') lt = lt - 1
    }
    try {
      chars.foreach((ch: Char) => { checkBrace(ch); if (lt < 0) throw AllDone })
    } catch {
      case AllDone =>
    }
    
    lt == 0
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {

    def giveANumber(money: Int, coins: List[Int]): Int = {

      if(coins.isEmpty) 0
      else if (money == 0) 1
      else if (money < 0) 0
      else giveANumber(money - coins.head, coins) + giveANumber(money, coins.tail)
    }
    //giveANumber(money, coins.sortWith((a, b) => a > b))
    giveANumber(money, coins)
  }
}
