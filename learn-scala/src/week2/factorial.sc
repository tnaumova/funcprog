object factorial {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  def factorial(n: Int): Int = {
    if (n == 0) 1
    else n * factorial(n-1)
  }                                               //> factorial: (n: Int)Int
  
  def sum(f: Int => Int)(a: Int, b: Int): Int = {
        def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a+1, acc + f(a))
        }
        loop(a, 0)
      }                                           //> sum: (f: Int => Int)(a: Int, b: Int)Int
      sum(x => x*x)(2,4)                          //> res0: Int = 29
}