object Rational {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(61); 
  println("Welcome to the Scala worksheet");$skip(20); val res$0 = 
  new Rational(1,2);System.out.println("""res0: Rational = """ + $show(res$0));$skip(28); 
  val x = new Rational(1,3);System.out.println("""x  : Rational = """ + $show(x ));$skip(28); 
  val y = new Rational(5,7);System.out.println("""y  : Rational = """ + $show(y ));$skip(28); 
  val z = new Rational(3,2);System.out.println("""z  : Rational = """ + $show(z ));$skip(18); val res$1 = 
  x.sub(y).sub(z);System.out.println("""res1: Rational = """ + $show(res$1));$skip(11); val res$2 = 
  x.add(x);System.out.println("""res2: Rational = """ + $show(res$2));$skip(18); val res$3 = 
  new Rational(2);System.out.println("""res3: Rational = """ + $show(res$3))}
}

class Rational(n : Int, d : Int){
  require(d != 0, "Denom should be > 0")
  
  def this(x : Int) = this(x, 1)
  
  val g = gcd(n,d)
  def numer = n
  def denom = d
  
  private def gcd(a: Int, b : Int): Int = if (b == 0) a else gcd(b,  a % b)
  
  def add(r : Rational) = new Rational(numer * r.denom + r.numer * denom, denom * r.denom)
  def neg = new Rational(-numer, denom)
  def sub(r: Rational) = add(r.neg)
  def less(r: Rational) = numer < r.numer && denom < r.denom
  override def toString = numer/gcd(numer,denom) + "/" + denom/gcd(numer,denom)
}