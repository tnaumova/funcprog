object factorial {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(62); 
  println("Welcome to the Scala worksheet");$skip(83); 
  def factorial(n: Int): Int = {
    if (n == 0) 1
    else n * factorial(n-1)
  };System.out.println("""factorial: (n: Int)Int""");$skip(196); 
  
  def sum(f: Int => Int)(a: Int, b: Int): Int = {
        def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a+1, acc + f(a))
        }
        loop(a, 0)
      };System.out.println("""sum: (f: Int => Int)(a: Int, b: Int)Int""");$skip(25); val res$0 = 
      sum(x => x*x)(2,4);System.out.println("""res0: Int = """ + $show(res$0))}
}