object Rational {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  new Rational(1,2)                               //> res0: Rational = 1/2
  val x = new Rational(1,3)                       //> x  : Rational = 1/3
  val y = new Rational(5,7)                       //> y  : Rational = 5/7
  val z = new Rational(3,2)                       //> z  : Rational = 3/2
  x.sub(y).sub(z)                                 //> res1: Rational = -79/42
  x.add(x)                                        //> res2: Rational = 2/3
  new Rational(2)                                 //> res3: Rational = 2/1
}

class Rational(n : Int, d : Int){
  require(d != 0, "Denom should be > 0")
  
  def this(x : Int) = this(x, 1)
  
  val g = gcd(n,d)
  def numer = n
  def denom = d
  
  private def gcd(a: Int, b : Int): Int = if (b == 0) a else gcd(b,  a % b)
  
  def add(r : Rational) = new Rational(numer * r.denom + r.numer * denom, denom * r.denom)
  def neg = new Rational(-numer, denom)
  def sub(r: Rational) = add(r.neg)
  def less(r: Rational) = numer < r.numer && denom < r.denom
  override def toString = numer/gcd(numer,denom) + "/" + denom/gcd(numer,denom)
}